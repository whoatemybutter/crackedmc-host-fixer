#!/usr/bin/env bash
# To make it executable, run "chmod +x ./fix_hosts.sh"
# To run, type "./fix_hosts.sh"
# Script requires "perl" to be installed for RegExp.
# You will be prompted for a password from sudo. Usually, your password will not show on the screen - this is normal.
# On MacOS the password is normally your system's password.

# Made by superpowers04#3887
# Forked by WhoAteMyButter
# With some syntax improvements by JustAGhost#2197

if grep -q "mojang" /etc/hosts; then
	echo -e "Your /etc/hosts file contains the following malicious text:\n------------------------------\n`grep -E "(authserver\.mojang\.com|sessionserver\.mojang\.com)" /etc/hosts`\n------------------------------\nThese redirect Mojang Authentication to an illegitimate address.\nThis script will now remove the Mojang redirects from your hosts file.\nTo abort this operation, press Ctrl+C. Otherwise, press any other key to proceed." && read
	sudo -H perl -i.bak -pe 's/(?:authserver\.mojang\.com|sessionserver\.mojang\.com)\s+(?:(?:[0-9]{1,3}[\.]){3}[0-9]{1,3})(?:|\n)//giu' /etc/hosts
	echo -e "A backup of the hosts file was made (at /etc/hosts.bak).\nOperation completed, try signing in again. If it worked, change your password.\nPress any key to exit." && read && exit
else
	echo "Hosts file is clean, no actions taken." && read && exit
fi